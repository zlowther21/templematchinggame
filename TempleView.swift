//
//  TempleView.swift
//  TempleMatching
//
//  Created by Zachary Lowther on 10/18/14.
//  Copyright (c) 2014 Zachary Lowther. All rights reserved.
//

import UIKit
class TempleView : UIView {
    
    // MARK: Constants
    
    func CORNER_RADIUS() -> CGFloat { return bounds.size.width * 0.03 }
    func CORNER_X_OFFSET() -> CGFloat { return bounds.size.width * 0.0556 }
    func CORNER_Y_OFFSET() -> CGFloat { return bounds.size.width * 0.0667 }
    
    // MARK: Properties
    var templeScaleFactor: CGFloat = 0.94
    var templeFileName: String?
    var templeName:String?
    var inMatchMode:Bool = true
    var canSelect:Bool = true
    var selected = false
  
    
    // MARK: Drawing
    
    override func drawRect(rect: CGRect) {
        drawTempleImageInFrame(self.templeFileName?, templeName: self.templeName?)
    }

    func drawTempleImageInFrame(templeFileName: String?, templeName: String?) {
        if let fileName = templeFileName {
            let roundedRect = UIBezierPath(roundedRect: bounds, cornerRadius: CORNER_RADIUS())
        
            roundedRect.addClip()
            UIColor(red:0, green:177.0/255.0, blue:106.0/255.0, alpha:1.0).setFill()
            UIRectFill(bounds)
            
            var templeImage = UIImage(named: fileName)

            
            if canSelect && selected {
                UIColor(red:0, green:122.0/255.0, blue:255.0/255.0, alpha:1.0).setFill()
                UIRectFill(bounds)
            }
            
            let imageRect = CGRectInset(self.bounds,
                bounds.size.width * (1.0 - templeScaleFactor),
                bounds.size.height * (1.0 - templeScaleFactor)
            )
                
            templeImage!.drawInRect(imageRect)
           
            
            if inMatchMode {
                if let name = templeName {
                    
                    var p = NSMutableParagraphStyle()
                    p.alignment = NSTextAlignment.Center
                    p.lineBreakMode = NSLineBreakMode.ByWordWrapping

                
                    
                    var templeTitle = NSAttributedString(string: name, attributes: [
                        NSForegroundColorAttributeName: UIColor.whiteColor(),
                        NSBackgroundColorAttributeName: UIColor.blackColor(),
                        NSParagraphStyleAttributeName: p,
                        NSFontAttributeName: UIFont(name: "Helvetica Neue", size: 9.0)!
                    ])
                
                    var nameHeight = templeTitle.size().height
                    if templeTitle.size().width > imageRect.size.width {
                        nameHeight = nameHeight * 2
                    }
                    var textBounds = CGRectMake(self.bounds.size.width * (1.0 - templeScaleFactor), self.bounds.size.height * (1.0 - templeScaleFactor), imageRect.size.width, nameHeight)
                    UIColor.blackColor().setFill()
                    UIRectFill(textBounds)
                    
                    var label = UILabel(frame: textBounds)
                    label.lineBreakMode = NSLineBreakMode.ByWordWrapping
                    label.numberOfLines = 2
                    
                    templeTitle.drawInRect(textBounds)
                   
                }
                
            }
            
            
            UIColor.whiteColor().setStroke()
            roundedRect.stroke()
        }
    }

}
