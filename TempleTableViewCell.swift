//
//  TempleTableViewCell.swift
//  TempleMatching
//
//  Created by Zachary Lowther on 10/18/14.
//  Copyright (c) 2014 Zachary Lowther. All rights reserved.
//

import UIKit
class TempleTableViewCell : UITableViewCell {
    
    @IBOutlet weak var templeNameLabel: UILabel!
}