//
//  ViewController.swift
//  TempleMatching
//
//  Created by Zachary Lowther on 10/18/14.
//  Copyright (c) 2014 Zachary Lowther. All rights reserved.
//

import UIKit

class TempleViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    // MARK: Outlets
    @IBOutlet weak var templeCollectionView: UICollectionView!
    @IBOutlet weak var templeTableView: UITableView!
    @IBOutlet weak var matchStatus: UIBarButtonItem!
    @IBOutlet weak var correctLabel: UIBarButtonItem!
    @IBOutlet weak var incorrectLabel: UIBarButtonItem!
    @IBOutlet weak var studyMatchButton: UIBarButtonItem!
    @IBOutlet weak var leftConstraint: NSLayoutConstraint!
    // MARK: Properties
    let FIXED_HEIGHT_SIZE:CGFloat = 110
    var game: TempleMatchingGame!
    var matchLabelShouldShow: Bool = false
    private lazy var randomTemples:[Temple] = []
    var correct: Int = 0 {
        didSet {
            correctLabel.title = "Correct: \(correct)"
        }
    }
    var incorrect: Int = 0 {
        didSet {
           incorrectLabel.title = "Incorrect: \(incorrect)"
        }
    }
    

    // MARK: View Did Load
    override func viewDidLoad() {
        setupGame()
        
    }
    
    // MARK: Custom Methods
    func setupGame() {
        game = TempleMatchingGame()
        randomTemples = []
        self.incorrect = 0
        self.correct = 0
        self.matchStatus.title = "Match Status"
        self.matchStatus.tintColor = UIColor.blackColor()
        self.templeCollectionView.contentInset = UIEdgeInsetsMake(5.0, 5.0, 5.0, 5.0);

        
        for i in 0 ..< game.getTempleCount() {
            randomTemples.append(game.getRandomTemple()!)
        }
        
        //deselectAll()
    }
    
    func updateUI(cell: TempleCollectionViewCell, indexPath: NSIndexPath) {
        let temple = randomTemples[indexPath.row]
        if temple.selected {
            self.templeCollectionView.selectItemAtIndexPath(indexPath, animated: true, scrollPosition: nil)
            cell.templeCustomView.selected = true
        }else {
            self.templeCollectionView.deselectItemAtIndexPath(indexPath, animated: true)
            cell.templeCustomView.selected = false
        }
       
        cell.templeCustomView.setNeedsDisplay()
    }

    func runMatch() {
    
        let collectionViewIndexPath = self.templeCollectionView.indexPathsForSelectedItems()
        if let tableViewIndexPath = self.templeTableView.indexPathForSelectedRow(){
            if collectionViewIndexPath.count > 0 {
                if let selectedImageIndexPath = collectionViewIndexPath[0] as? NSIndexPath{
                    let wonGame = game.match(game.getTempleAtIndex(tableViewIndexPath.row), temple2: randomTemples[selectedImageIndexPath.row])
                    if wonGame == true{
                        self.matchStatus.title = "Correct Match : \(game.getTempleAtIndex(tableViewIndexPath.row).templeName)"
                        self.matchStatus.tintColor = UIColor(red:0, green:177.0/255.0, blue:106.0/255.0, alpha:1.0)
                        self.correct++
                        deselectTableView()
                        game.removeTempleAtIndex(tableViewIndexPath.row)
                        randomTemples.removeAtIndex(selectedImageIndexPath.row)
                        self.templeTableView.deleteRowsAtIndexPaths([tableViewIndexPath], withRowAnimation: UITableViewRowAnimation.Fade)
                        self.templeCollectionView.deleteItemsAtIndexPaths([selectedImageIndexPath])
                    }else {
                        self.matchStatus.title = "Incorrect :("
                        self.matchStatus.tintColor = UIColor.redColor()
                        self.incorrect++
                        deselectTableView()
                    }
                   
                    
                }
            }
        }
        
    }
    
    func deselectTableView() {
        let collectionViewIndexPath = self.templeCollectionView.indexPathsForSelectedItems()
        if let tableViewIndexPath = self.templeTableView.indexPathForSelectedRow(){
            self.templeTableView.deselectRowAtIndexPath(tableViewIndexPath, animated: true)

        }

    }

    func deselectCollectionView() {
        let collectionViewIndexPath = self.templeCollectionView.indexPathsForSelectedItems()
        if collectionViewIndexPath.count > 0 {
            if let selectedImageIndexPath = collectionViewIndexPath[0] as? NSIndexPath{
                self.templeCollectionView.deselectItemAtIndexPath(selectedImageIndexPath, animated: true)
             
                var visibleIndexPaths = self.templeCollectionView.indexPathsForVisibleItems()
                for visibleIndexPath in visibleIndexPaths {
                 
                    if visibleIndexPath.row == selectedImageIndexPath.row {
                        (self.templeCollectionView.cellForItemAtIndexPath(selectedImageIndexPath) as TempleCollectionViewCell).templeCustomView.selected = false
                        (self.templeCollectionView.cellForItemAtIndexPath(selectedImageIndexPath) as TempleCollectionViewCell).templeCustomView.setNeedsDisplay()
                        break
                    }
                }
               
    
                
                
                self.randomTemples[selectedImageIndexPath.row].selected = false
                
            }
        }
        
    }
    // MARK: Table View Data Source Methods
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("TempleTableCell") as TempleTableViewCell
        cell.templeNameLabel?.text = game.getTempleAtIndex(indexPath.row).templeName
        return cell
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return game.getTempleCount()
    }
    
    // MARK: Table View Delegate Methods
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        runMatch()
        
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Temples"
    }
  
    
    
    
    // MARK: Collection View Data Source Methods
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        var cell = collectionView.dequeueReusableCellWithReuseIdentifier("TempleCell", forIndexPath: indexPath) as TempleCollectionViewCell
        

        cell.templeCustomView.templeFileName = randomTemples[indexPath.row].templeFileName
        
        if matchLabelShouldShow {
            cell.templeCustomView.inMatchMode = true
            cell.templeCustomView.templeName = randomTemples[indexPath.row].templeName
            cell.templeCustomView.canSelect = false

        }else {
            cell.templeCustomView.inMatchMode = false
            cell.templeCustomView.templeName = nil
            cell.templeCustomView.canSelect = true
        }
        updateUI(cell, indexPath: indexPath )
        
        return cell
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return game.getTempleCount()
    }
    
    
    
    
    // MARK: Collection View Delegate Methods
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {

        //deselectAll()
        randomTemples[indexPath.row].selected = !randomTemples[indexPath.row].selected
        collectionView.reloadItemsAtIndexPaths([indexPath])
        
        runMatch()

        
    }

    func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        deselectCollectionView()
        return true
    }
    
    // MARK: Collection View Delegate Flow Layout Methods
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let size = UIImage(named: randomTemples[indexPath.row].templeFileName)!.size
        
        let fixedHeightRatio = FIXED_HEIGHT_SIZE / size.height
        return CGSizeMake(size.width * fixedHeightRatio, FIXED_HEIGHT_SIZE)
        
    }

    
    // MARK: IBActions
    @IBAction func dealButtonPressed(sender: UIBarButtonItem) {
        
        
        if self.templeTableView.hidden {
            studyOrMatchDidPress(sender)
        }else {
            setupGame()
            self.templeCollectionView.reloadSections(NSIndexSet(index: 0))
            self.templeTableView.reloadSections(NSIndexSet(index: 0), withRowAnimation: UITableViewRowAnimation.Fade)

        }
    }
    
    @IBAction func studyOrMatchDidPress(sender: UIBarButtonItem) {
    
        self.view.layoutIfNeeded()
        UIView.transitionWithView(self.templeTableView, duration: 0.7, options: UIViewAnimationOptions.TransitionCurlDown, animations: {}, completion: nil)
        UIView.transitionWithView(self.templeCollectionView, duration: 0.7, options: UIViewAnimationOptions.TransitionCurlDown, animations: {}, completion: nil)
        UIView.animateWithDuration(0.5, delay: 0, options: .CurveEaseInOut, animations: {
            
            if self.studyMatchButton.title! == "Study" {
                self.templeTableView.hidden = true
                self.studyMatchButton.title = "Match"
                self.deselectCollectionView()
                self.deselectTableView()
                self.matchLabelShouldShow = true
            }else {
                self.templeTableView.hidden = false
                self.studyMatchButton.title = "Study"
                self.matchLabelShouldShow = false
            }
            
            if self.templeTableView.hidden {
                self.setupGame()
                self.leftConstraint.constant = 0
                self.templeCollectionView.reloadData()
            }else {
                self.setupGame()
                self.leftConstraint.constant = self.templeTableView.frame.size.width
                self.templeTableView.reloadData()
                self.templeCollectionView.reloadData()
            }
            
        }, completion: nil)
        

        

    }

    
}

