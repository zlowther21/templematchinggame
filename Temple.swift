//
//  TempleImage.swift
//  TempleMatching
//
//  Created by Zachary Lowther on 10/18/14.
//  Copyright (c) 2014 Zachary Lowther. All rights reserved.
//

import Foundation
class Temple : Image{
    
    var templeName: String
    var templeFileName: String
    
    init(filename: String, name: String) {
        self.templeName = name
        self.templeFileName = filename
        
    }
}