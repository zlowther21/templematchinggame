I added the following temple images:
- Manti Utah : I served part of my mission in Manti
- Orlando Florida : Two of my siblings were married in this temple
- Provo Utah : I have been a worker in this temple
- Salt Lake : My oldest sibling was married in this temple
- St. George Utah : I served part of my mission in St. George. I also did my granddaddy's endowment in this temple
- Washington D.C. : My parents were sealed in this temple